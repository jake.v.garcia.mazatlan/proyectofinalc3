import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
const firebaseConfig = {
    apiKey: "AIzaSyCAjSyYLBZZOozIOaoMXfcB5hGRpWYfvc8",
    authDomain: "proyectopaginawebc3.firebaseapp.com",
    projectId: "proyectopaginawebc3",
    storageBucket: "proyectopaginawebc3.appspot.com",
    messagingSenderId: "882016434455",
    appId: "1:882016434455:web:a0a018baa7d11ec41adf52",
    measurementId: "G-9FEX6JRX88"
  };
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

const formulario = document.getElementById("formulario");
const emailInput = document.getElementById("email");
const passwordInput = document.getElementById("password");
const signInButton = document.getElementById("button");
const errorMensaje = document.getElementById("errorMensaje");


formulario.addEventListener("submit", (event) => {
    event.preventDefault();
  
    const email = emailInput.value;
    const password = passwordInput.value;
  
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
  
        window.location.href = "/HTML/opciones.html";
      })
      .catch((error) => {
  
        console.log("Error al iniciar sesión:", error);
  
        errorMensaje.textContent = "Credenciales incorrectas. Por favor, intenta nuevamente.";
      });
  });
  
  emailInput.addEventListener("click", () => {
    errorMensaje.textContent = "";
  });
  
  passwordInput.addEventListener("click", () => {
    errorMensaje.textContent = "";
  });
  