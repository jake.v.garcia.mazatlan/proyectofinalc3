import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";

const firebaseConfig = {
    apiKey: "AIzaSyCAjSyYLBZZOozIOaoMXfcB5hGRpWYfvc8",
    authDomain: "proyectopaginawebc3.firebaseapp.com",
    projectId: "proyectopaginawebc3",
    storageBucket: "proyectopaginawebc3.appspot.com",
    messagingSenderId: "882016434455",
    appId: "1:882016434455:web:a0a018baa7d11ec41adf52",
    measurementId: "G-9FEX6JRX88"
  };

window.addEventListener('DOMContentLoaded', (event) => {
    mostrarProductosHTML();
  });
  
function mostrarProductosHTML() {
    const dbRef = ref(db, 'productos');
    const section = document.querySelector('.products-grid');
  
    onValue(dbRef, (snapshot) => {
      section.innerHTML = ''; // Limpiar contenido anterior
  
      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const data = childSnapshot.val();
  
        const productDiv = document.createElement('div');
        productDiv.className = 'product';
  
        const productImage = document.createElement('img');
        productImage.src = data.UrlImg;
        productImage.alt = '';
        productDiv.appendChild(productImage);
  
        const productTitle = document.createElement('h3');
        productTitle.className = 'product-title';
        productTitle.textContent = data.Nombre;
        productDiv.appendChild(productTitle);
  
        const productPrice = document.createElement('p');
        productPrice.className = 'product-price';
        productPrice.textContent = `$${data.Precio}`;
        productDiv.appendChild(productPrice);
  
        const productStock = document.createElement('p');
        productStock.className = 'product-stock';
        productStock.textContent = `Disponible: ${data.Cantidad} unidades`;
        productDiv.appendChild(productStock);
  
        const addToCartBtn = document.createElement('button');
        addToCartBtn.className = 'add-to-cart';
        addToCartBtn.textContent = 'Agregar al carrito';
        productDiv.appendChild(addToCartBtn);
  
        section.appendChild(productDiv);
      });
    }, { onlyOnce: true });
  }
